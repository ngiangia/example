library ieee;
use ieee.std_logic_1164.all;

package my_lib is
  function log2ceil(arg : positive) return natural;
  function div_ceil(a : natural; b : positive) return natural;
end my_lib;

package body my_lib is
  
-- Logarithms: log*ceil*
-- ==========================================================================
-- return log2; always rounded up
-- From https://github.com/VLSI-EDA/PoC/blob/master/src/common/utils.vhdl
  function log2ceil(arg : positive) return natural is
      variable tmp : positive;
      variable log : natural;
  begin
      if arg = 1 then return 0; end if;
      tmp := 1;
      log := 0;
      while arg > tmp loop
          tmp := tmp * 2;
          log := log + 1;
      end loop;
      return log;
  end function;

-- Divisions: div_*
-- ===========================================================================
-- integer division; always round-up
  function div_ceil(a : natural; b : positive) return natural is        -- calculates: ceil(a / b)
  begin
    return (a + (b - 1)) / b;
  end function;

end package body my_lib;
