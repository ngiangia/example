import os
import git

def parse_configuration(PATH, CONFIG_PATHS):
	lines = [line.rstrip('\n') for line in open(PATH + "/CONFIGURE.txt")]
	for line in lines:
		if '=' in line:
			CONFIG_PATHS[0].append(line.split('=')[0])
			CONFIG_PATHS[1].append(line.split('=')[-1])
	
						
def write_VHDL_package(GIT_SHA, SRC_PATH, MAIN_PATH):
	print GIT_SHA
	GIT_SHA_0 = GIT_SHA[0:8]
	GIT_SHA_1 = GIT_SHA[8:16]
	GIT_SHA_2 = GIT_SHA[16:24]
	GIT_SHA_3 = GIT_SHA[24:32]
	GIT_SHA_4 = GIT_SHA[32:40]


	try:
		os.stat(SRC_PATH + "/common")
	except:
		os.mkdir(SRC_PATH + "/common")   

	pkg_file = open(SRC_PATH + "/common/base_package.vhd","w")
	pkg_file.write("library ieee;\n")
	pkg_file.write("use ieee.std_logic_1164.all;\n\n")
	pkg_file.write("package base_pkg is\n")
	pkg_file.write("\tconstant MAIN_PATH  : string := \"" + MAIN_PATH + "\";\n")
	pkg_file.write("\tconstant GIT_SHA_0  : std_logic_vector(31 downto 0)  := X\"" + str(GIT_SHA_0) + "\";\n")
	pkg_file.write("\tconstant GIT_SHA_1  : std_logic_vector(31 downto 0)  := X\"" + str(GIT_SHA_1) + "\";\n")
	pkg_file.write("\tconstant GIT_SHA_2  : std_logic_vector(31 downto 0)  := X\"" + str(GIT_SHA_2) + "\";\n")
	pkg_file.write("\tconstant GIT_SHA_3  : std_logic_vector(31 downto 0)  := X\"" + str(GIT_SHA_3) + "\";\n")
	pkg_file.write("\tconstant GIT_SHA_4  : std_logic_vector(31 downto 0)  := X\"" + str(GIT_SHA_4) + "\";\n")
 	pkg_file.write("end package;\n")
	pkg_file.close()


def write_CFG_script(SCRIPT_PATH, CONFIG_PATHS):
	setup_file = open(SCRIPT_PATH + "/setup.sh","w")
	for i in range(len(CONFIG_PATHS[0])):
		#if CONFIG_PATHS[0][i] == "XILINX_INST_PATH":
		setup_file.write("export " + CONFIG_PATHS[0][i] + "=" + CONFIG_PATHS[1][i] + "\n")

	setup_file.close()
	

def main():

	CONFIG_PATHS= [[],[]]
	SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))
	CONFIG_PATHS[0].append("SCRIPT_PATH")
	CONFIG_PATHS[1].append(SCRIPT_PATH)
	MAIN_PATH = os.path.dirname(os.path.realpath(SCRIPT_PATH))
	CONFIG_PATHS[0].append("MAIN_PATH")
	CONFIG_PATHS[1].append(MAIN_PATH)
	SRC_PATH = MAIN_PATH + "/src"
	CONFIG_PATHS[0].append("SRC_PATH")
	CONFIG_PATHS[1].append(SRC_PATH)
	TB_PATH = MAIN_PATH + "/tb"	
	CONFIG_PATHS[0].append("TB_PATH")
	CONFIG_PATHS[1].append(TB_PATH)


	GIT_REPO = git.Repo(search_parent_directories=True)
	GIT_SHA = GIT_REPO.head.object.hexsha

	
	parse_configuration(MAIN_PATH, CONFIG_PATHS)

	write_VHDL_package(GIT_SHA, SRC_PATH, MAIN_PATH)

	write_CFG_script(SCRIPT_PATH, CONFIG_PATHS)

if __name__=="__main__":
    main()
