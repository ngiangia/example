library ieee;
use ieee.std_logic_1164.all;

package base_pkg is
	constant MAIN_PATH  : string := "/media/ngiangia/c5e61e51-aaff-448a-a459-7d1f913b6f07/HTT/example";
	constant GIT_SHA_0  : std_logic_vector(31 downto 0)  := X"024da198";
	constant GIT_SHA_1  : std_logic_vector(31 downto 0)  := X"94ce8d13";
	constant GIT_SHA_2  : std_logic_vector(31 downto 0)  := X"7d7d391a";
	constant GIT_SHA_3  : std_logic_vector(31 downto 0)  := X"23540ff5";
	constant GIT_SHA_4  : std_logic_vector(31 downto 0)  := X"5cd2233c";
end package;
