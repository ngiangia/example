----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/24/2019 03:15:22 PM
-- Design Name: 
-- Module Name: Dummy_inverter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Dummy_inverter is
Port ( 
	clk 			: in STD_LOGIC;
	rst 			: in STD_LOGIC;
	ready			: out STD_LOGIC;
	data_in 		: in STD_LOGIC_VECTOR (63 downto 0);
	data_in_valid	: in std_logic;
	data_o 			: out STD_LOGIC_VECTOR (63 downto 0);
	data_o_valid	: out std_logic
);
end Dummy_inverter;

architecture Behavioral of Dummy_inverter is


--########################################################################
--##                                                                    ##
--##                       SIGNALS DECLARATION                          ##
--##                                                                    ##
--########################################################################
	signal data_fifo								: std_logic_vector(63 downto 0);
	signal data_fifo_valid							: std_logic;
	signal fifo_full, fifo_wr_busy, fifo_rd_busy	: std_logic;
	signal fifo_rd_en								: std_logic;


--########################################################################
--##                                                                    ##
--##                     COMPONENT DECLARATION                          ##
--##                                                                    ##
--########################################################################

	COMPONENT Dummy_FIFO
	PORT (
		clk 		: IN STD_LOGIC;
		srst 		: IN STD_LOGIC;
		din 		: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
		wr_en 		: IN STD_LOGIC;
		rd_en 		: IN STD_LOGIC;
		dout 		: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
		full 		: OUT STD_LOGIC;
		empty 		: OUT STD_LOGIC;
		valid 		: OUT STD_LOGIC;
		wr_rst_busy : OUT STD_LOGIC;
		rd_rst_busy : OUT STD_LOGIC
	);
	END COMPONENT;

begin
ready		<= not (fifo_full or fifo_rd_busy or fifo_wr_busy);
fifo_rd_en	<= not rst and not fifo_rd_busy;
--########################################################################
--##                                                                    ##
--##                     COMPONENT IMPLEMENTATION                       ##
--##                                                                    ##
--########################################################################
	Dummy_FIFO_impl: Dummy_FIFO
	Port Map (
		clk 		=> clk,
		srst 		=> rst,
		din 		=> data_in,
		wr_en 		=> data_in_valid,
		rd_en 		=> fifo_rd_en,
		dout 		=> data_fifo,
		full 		=> fifo_full,
		empty 		=> open,
		valid 		=> data_fifo_valid,
		wr_rst_busy => fifo_wr_busy,
		rd_rst_busy => fifo_rd_busy
	);


	inverter_proc: process (rst, clk)
	begin
		if(rst = '1') then
			data_o_valid		<= '0';
		elsif(rising_edge(clk)) then
			data_o				<= not data_fifo;
			data_o_valid		<= data_fifo_valid;
		end if;
	end process inverter_proc;

end Behavioral;
